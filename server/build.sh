#!/bin/sh

mvn clean package

if [ $? -eq 0 ]
then
  echo "Building docker file now.."
  docker build -t wschneider/chat-server .
else
  echo "build failed"
fi