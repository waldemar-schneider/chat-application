package de.wschneider.chatserver;

import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Stream;

import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;

import lombok.Setter;

public class TestSessionHandler extends StompSessionHandlerAdapter {

    private final AtomicReference<Throwable> failure;
    @Setter
    private String topicMessages;
    @Setter
    private StompFrameHandler frameHandler;

    TestSessionHandler(AtomicReference<Throwable> failure) {
        this.failure = failure;
    }

    @Override
    public void handleFrame(StompHeaders headers, Object payload) {
        this.failure.set(new Exception(headers.toString()));
    }

    @Override
    public void afterConnected(StompSession session, StompHeaders connectedHeaders) {
        ensureFieldsAreInitialized();
        session.subscribe(topicMessages, frameHandler);
    }

    @Override
    public void handleException(StompSession s, StompCommand c, StompHeaders h, byte[] p, Throwable ex) {
        this.failure.set(ex);
    }

    @Override
    public void handleTransportError(StompSession session, Throwable ex) {
        this.failure.set(ex);
    }

    private void ensureFieldsAreInitialized() {
        boolean anyFieldNotInitialized = Stream.of(failure, topicMessages, frameHandler)
            .anyMatch(Objects::isNull);

        if (anyFieldNotInitialized) {
            throw new IllegalStateException("Fields are not initialized");
        }
    }
}
