package de.wschneider.chatserver;


import static de.wschneider.chatserver.MessageController.MAPPING_INBOUND_MESSAGES;
import static de.wschneider.chatserver.MessageController.MAPPING_INBOUND_SIGNIN_MESSAGES;
import static de.wschneider.chatserver.MessageController.MAPPING_INBOUND_SIGNOFF_MESSAGES;
import static de.wschneider.chatserver.MessageController.TOPIC_OUTBOUND_CHAT_MESSAGES;
import static de.wschneider.chatserver.MessageController.TOPIC_OUTBOUND_SYSTEM_MESSAGES;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.socket.WebSocketHttpHeaders;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;
import org.springframework.web.socket.sockjs.client.SockJsClient;
import org.springframework.web.socket.sockjs.client.Transport;
import org.springframework.web.socket.sockjs.client.WebSocketTransport;

import lombok.SneakyThrows;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ChatMessageIntegrationTest {

    @LocalServerPort
    private int port;

    private SockJsClient sockJsClient;

    private WebSocketStompClient stompClient;

    private final WebSocketHttpHeaders headers = new WebSocketHttpHeaders();

    private CountDownLatch latch;
    private AtomicReference<Throwable> failure;
    private Consumer<Object> consumer;
    private Message chatPayloadToFire;
    private String websocketUrl;
    private TestSessionHandler handler;
    private String user;

    @BeforeEach
    public void setup() {
        List<Transport> transports = List.of(new WebSocketTransport(new StandardWebSocketClient()));
        sockJsClient = new SockJsClient(transports);

        stompClient = new WebSocketStompClient(sockJsClient);
        stompClient.setMessageConverter(new MappingJackson2MessageConverter());

        latch = new CountDownLatch(1);
        failure = new AtomicReference<>();
        websocketUrl = "ws://localhost:{port}/chat/";
    }

    @Test
    public void should_send_message_to_topic_and_verify() {

        givenMessagePayload();
        givenInitializedStompHandler();
        givenChatResponseVerification();

        whenChatMessageIsSent();

        thenVerifyResponseMessage();
    }

    @Test
    public void should_sign_on_message_to_topic_and_verify() {

        givenSignOnOrOffMessagePayload();
        givenInitializedStompHandler();
        givenSystemResponseVerification();

        whenSignOnMessageIsSent();

        thenVerifyResponseMessage();
    }

    @Test
    public void should_sign_off_message_to_topic_and_verify() {

        givenSignOnOrOffMessagePayload();
        givenInitializedStompHandler();
        givenSystemResponseVerification();

        whenSignOffMessageIsSent();

        thenVerifyResponseMessage();
    }

    private void givenInitializedStompHandler() {
        handler = new TestSessionHandler(failure);
    }

    private void givenMessagePayload() {
        String sender = "Waldemar";
        String text = "Hello there!";

        chatPayloadToFire = new Message(sender, text);
    }

    private void givenSignOnOrOffMessagePayload() {
        user = "Waldemar";
    }

    private void givenChatResponseVerification() {
        handler.setTopicMessages(TOPIC_OUTBOUND_CHAT_MESSAGES);
        handler.setFrameHandler(new ChatMessageFrameHandler() {

            @Override
            public void handleFrame(StompHeaders headers, Object payload) {
                try {
                    assertThat(payload).isNotNull();
                    assertThat(payload).isInstanceOf(MessagePayload.class);

                    MessagePayload map = (MessagePayload) payload;
                    assertThat(map.getSender()).isEqualTo(chatPayloadToFire.getSender());
                    assertThat(map.getText()).isEqualTo(chatPayloadToFire.getText());
                    assertThat(map.getTimestamp()).isNotNull();
                } catch (Throwable t) {
                    failure.set(t);
                } finally {
                    latch.countDown();
                }
            }
        });
    }

    private void givenSystemResponseVerification() {
        handler.setTopicMessages(TOPIC_OUTBOUND_SYSTEM_MESSAGES);
        handler.setFrameHandler(new SystemMessageFrameHandler() {

            @Override
            public void handleFrame(StompHeaders headers, Object payload) {
                try {
                    assertThat(payload).isNotNull();
                    assertThat(payload).isInstanceOf(SystemPayload.class);

                    SystemPayload map = (SystemPayload) payload;
                    assertThat(map.getUser()).isEqualTo(user);
                    assertThat(map.getTimestamp()).isNotNull();
                } catch (Throwable t) {
                    failure.set(t);
                } finally {
                    latch.countDown();
                }
            }
        });
    }

    @SneakyThrows
    private void whenChatMessageIsSent() {
        StompSession session = connectAndRetrieveSession();
        try {
            session.send("/app" + MAPPING_INBOUND_MESSAGES, chatPayloadToFire);
        } catch (Throwable t) {
            failure.set(t);
            latch.countDown();
        }
    }

    @SneakyThrows
    private void whenSignOnMessageIsSent() {
        StompSession session = connectAndRetrieveSession();
        try {
            session.send("/app" + MAPPING_INBOUND_SIGNIN_MESSAGES, user);
        } catch (Throwable t) {
            failure.set(t);
            latch.countDown();
        }
    }

    @SneakyThrows
    private void whenSignOffMessageIsSent() {
        StompSession session = connectAndRetrieveSession();
        try {
            session.send("/app" + MAPPING_INBOUND_SIGNOFF_MESSAGES, user);
        } catch (Throwable t) {
            failure.set(t);
            latch.countDown();
        }
    }

    @SneakyThrows
    private void thenVerifyResponseMessage() {
        if (latch.await(1, SECONDS)) {
            if (failure.get() != null) {
                throw new AssertionError("", failure.get());
            }
        } else {
            fail("Message not received");
        }
    }

    private StompSession connectAndRetrieveSession() throws InterruptedException, java.util.concurrent.ExecutionException, java.util.concurrent.TimeoutException {
        ListenableFuture<StompSession> connect = stompClient.connect(websocketUrl, handler, this.port);
        return connect.get(1, SECONDS);
    }

}
