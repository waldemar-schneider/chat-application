package de.wschneider.chatserver;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import lombok.Getter;

/**
 * This class functions as a simple session store. Means, it only saves temporary logged in users, so new users
 * can be notified who is already there.
 */
@Component
public class UserSessionStore {

    @Getter
    private List<String> users = new ArrayList<>();

    public void addUser(String user) {
        if (users.contains(user)) {
            throw new IllegalStateException("User already logged in");
        }
        users.add(user);
    }

    public void removeUser(String user) {
        users.remove(user);
    }

}
