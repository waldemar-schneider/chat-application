package de.wschneider.chatserver;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class SystemPayload {

    private String user;
    private Status status;
    private String timestamp;
    private List<String> loggedInUsers;

    public enum Status {
        SIGN_IN, SIGN_OFF
    }
}
