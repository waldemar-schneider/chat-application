package de.wschneider.chatserver;

import static de.wschneider.chatserver.SystemPayload.Status.SIGN_IN;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;

import lombok.AllArgsConstructor;

@Controller
@AllArgsConstructor
public class MessageController {

    static final String MAPPING_INBOUND_MESSAGES = "/chat";
    static final String TOPIC_OUTBOUND_CHAT_MESSAGES = "/topic/chat-messages";
    static final String TOPIC_OUTBOUND_SYSTEM_MESSAGES = "/topic/system-messages";

    private static final Logger LOGGER = LoggerFactory.getLogger(MessageController.class);
    public static final String MAPPING_INBOUND_SIGNIN_MESSAGES = "/sign-in";
    public static final String MAPPING_INBOUND_SIGNOFF_MESSAGES = "/sign-off";

    private final UserSessionStore userSessionStore;

    @MessageMapping(MAPPING_INBOUND_MESSAGES)
    @SendTo(TOPIC_OUTBOUND_CHAT_MESSAGES)
    public MessagePayload send(@Validated Message message) {
        LOGGER.info("Retrieving payload: {}", message.toString());
        String timestamp = new SimpleDateFormat("HH:mm:SS").format(new Date());

        MessagePayload messagePayload = new MessagePayload(message.getSender(), message.getText(), timestamp);
        LOGGER.info("Sending payload: {}", messagePayload.toString());

        return messagePayload;
    }

    @MessageMapping(MAPPING_INBOUND_SIGNIN_MESSAGES)
    @SendTo(TOPIC_OUTBOUND_SYSTEM_MESSAGES)
    public SystemPayload signIn(String user) {
        String timestamp = new SimpleDateFormat("HH:mm:SS").format(new Date());
        SystemPayload systemPayload = new SystemPayload(user, SIGN_IN, timestamp, userSessionStore.getUsers());

        userSessionStore.addUser(user);
        LOGGER.info("Sending system payload: {}", systemPayload.toString());

        return systemPayload;
    }

    @MessageMapping(MAPPING_INBOUND_SIGNOFF_MESSAGES)
    @SendTo(TOPIC_OUTBOUND_SYSTEM_MESSAGES)
    public SystemPayload signOff(String user) {
        String timestamp = new SimpleDateFormat("HH:mm:SS").format(new Date());
        userSessionStore.removeUser(user);

        SystemPayload systemPayload = new SystemPayload(user, SystemPayload.Status.SIGN_OFF, timestamp, userSessionStore.getUsers());
        LOGGER.info("Sending system payload: {}", systemPayload.toString());

        return systemPayload;
    }
}
