package de.wschneider.chatclient;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InOrder;
import org.mockito.Mockito;
import org.mockito.internal.InOrderImpl;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandler;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.socket.messaging.WebSocketStompClient;

import lombok.SneakyThrows;

class ChatClientTest {

    private ChatStompSessionHandler chatStompSessionHandler = Mockito.mock(ChatStompSessionHandler.class);
    private WebSocketStompClient stompClient = Mockito.mock(WebSocketStompClient.class);
    private UserContext userContext = Mockito.mock(UserContext.class);
    private StompSession stompSession = Mockito.mock(StompSession.class);
    private KeyboardReader keyboardReader = Mockito.mock(KeyboardReader.class);
    private InOrder inOrder;

    private ChatClient client;

    @BeforeEach
    void setUp() {
        client = new ChatClient(chatStompSessionHandler, userContext, keyboardReader, stompClient);
        client.setChatMapping("/chat");
        givenStompSession();
        inOrder = new InOrderImpl(List.of(stompSession));
    }

    @Test
    @SneakyThrows
    public void should_update_user_context() {
        // given
        givenNameInput();
        givenExitInput();

        // when
        whenClientIsStarted();

        // then
        thenVerifyUserContextIsUpdated();
    }

    @Test
    @SneakyThrows
    public void should_handle_chat_input() {
        // given
        givenInput("Waldemar", "Hello there!", "exit");

        // when
        whenClientIsStarted();

        // then
        thenVerifySignInMessageIsSent();
        thenVerifyMessageIsSent();
        thenVerifySignOffMessageIsSent();
    }

    private void givenChatInput() {
        givenInput("Hello there!");
    }

    private void givenExitInput() {
        givenInput("exit");
    }

    private void givenNameInput() {
        givenInput("Waldemar");
    }

    private void givenInput(String firstInput, String... input) {
        when(keyboardReader.readInput()).thenReturn(firstInput, input);
    }

    @SneakyThrows
    private void givenStompSession() {
        ListenableFuture<StompSession> listenableFuture = Mockito.mock(ListenableFuture.class);
        when(stompClient.connect(any(String.class), any(StompSessionHandler.class), any())).thenReturn(listenableFuture);
        when(listenableFuture.get(any(Long.class), any(TimeUnit.class))).thenReturn(stompSession);
    }

    private void whenClientIsStarted() {
        client.start();
    }

    private void thenVerifySignInMessageIsSent() {
        inOrder.verify(stompSession).send(eq("/app/sign-in"), eq("Waldemar"));
    }

    private void thenVerifySignOffMessageIsSent() {
        inOrder.verify(stompSession).send(eq("/app/sign-off"), eq("Waldemar"));
    }

    private void thenVerifyMessageIsSent() {
        inOrder.verify(stompSession).send(eq("/app/chat"), any(Message.class));
    }

    private void thenVerifyUserContextIsUpdated() {
        verify(userContext).setUser(any(String.class));
    }


}