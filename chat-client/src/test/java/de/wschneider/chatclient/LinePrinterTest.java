package de.wschneider.chatclient;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class LinePrinterTest {

    private UserContext userContext;
    private LinePrinter linePrinter;
    private MessageResponsePayload messageResponsePayload;
    private SystemPayload systemPayload;
    private String user;
    private PrintStream printStream = Mockito.mock(PrintStream.class);

    @Test
    void should_log_chat_from_same_user() {
        // given
        givenLoggedInUser();
        givenChatMessageFromSameUser();

        // when
        whenChatIsLogged();

        // then
        verifyPrintInteraction();
    }

    @Test
    void should_log_chat_from_another_user() {
        // given
        givenLoggedInUser();
        givenChatMessageFromAnotherUser();

        // when
        whenChatIsLogged();

        // then
        verifyPrintInteraction();
    }

    @Test
    void should_not_log_chat_from_another_user_while_signing_off() {
        // given
        givenSigningOffUser();
        givenChatMessageFromAnotherUser();

        // when
        whenChatIsLogged();

        // then
        verifyNoInteraction();
    }

    @Test
    void should_not_log_sign_on_from_same_user() {
        // given
        givenLoggedInUser();
        givenSignUpPayloadFromSameUser();

        // when
        whenSystemPayloadIsConsumed();

        // then
        verifyNoSystemLogInteraction();
    }

    @Test
    void should_log_sign_on_from_different_user() {
        // given
        givenLoggedInUser();
        givenSignUpPayloadFromDifferentUser();

        // when
        whenSystemPayloadIsConsumed();

        // then
        verifySystemLogInteraction();
    }

    @Test
    void should_not_log_sign_off_from_same_user() {
        // given
        givenSigningOffUser();
        givenSignOffPayloadFromUser();

        // when
        whenSystemPayloadIsConsumed();

        // then
        verifyNoSystemLogInteraction();
    }

    @Test
    void should_log_sign_off_from_different_user() {
        // given
        givenSigningOffFromDifferentUser();
        givenSignOffPayloadFromUser();

        // when
        whenSystemPayloadIsConsumed();

        // then
        verifyNoSystemLogInteraction();
    }

    private void givenSignUpPayloadFromSameUser() {
        systemPayload = new SystemPayload(user, SystemPayload.Status.SIGN_IN, givenNowTimestamp());
    }

    private void givenSignOffPayloadFromUser() {
        systemPayload = new SystemPayload(user, SystemPayload.Status.SIGN_OFF, givenNowTimestamp());
    }

    private void givenSignUpPayloadFromDifferentUser() {
        systemPayload = new SystemPayload("Olaf", SystemPayload.Status.SIGN_IN, givenNowTimestamp());
    }

    private void givenLoggedInUser() {
        user = "Waldemar";
        userContext = new UserContext(user, false);
    }

    private void givenSigningOffUser() {
        user = "Waldemar";
        userContext = new UserContext(user, true);
    }

    private void givenSigningOffFromDifferentUser() {
        user = "Olaf";
        userContext = new UserContext(user, true);
    }

    private void givenChatMessageFromSameUser() {
        messageResponsePayload = new MessageResponsePayload(user, "Hello there!", givenNowTimestamp());
    }

    private void givenChatMessageFromAnotherUser() {
        messageResponsePayload = new MessageResponsePayload("Olaf", "General Kenobi!", givenNowTimestamp());
    }

    private void whenChatIsLogged() {
        linePrinter = new LinePrinter(userContext, printStream);
        linePrinter.logChat(messageResponsePayload);
    }

    private void whenSystemPayloadIsConsumed() {
        linePrinter = new LinePrinter(userContext, printStream);
        linePrinter.logUserSignOnOrOff(systemPayload);
    }

    private void verifyNoInteraction() {
        verify(printStream, never()).println(any(String.class));
    }

    private void verifyPrintInteraction() {
        verify(printStream, times(1)).println(any(String.class));
    }

    private void verifyNoSystemLogInteraction() {
        verify(printStream, never()).println(any(String.class));
    }

    private void verifySystemLogInteraction() {
        verify(printStream, times(1)).println(any(String.class));
    }

    private String givenNowTimestamp() {
        return new SimpleDateFormat("HH:mm:SS").format(new Date());
    }
}