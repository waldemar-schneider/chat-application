package de.wschneider.chatclient;

import java.io.PrintStream;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ChatConfiguration {

    @Bean
    public PrintStream printStream() {
        return System.out;
    }
}
