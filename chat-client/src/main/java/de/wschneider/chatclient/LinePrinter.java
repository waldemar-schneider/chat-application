package de.wschneider.chatclient;

import java.io.PrintStream;

import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
class LinePrinter {

    private final UserContext userContext;
    private final PrintStream printStream;

    void logChat(MessageResponsePayload payload) {
        if (!userContext.isSignOffInitiated()) {
            printStream.println(String.format("%s %s: %s", payload.getTimestamp(), payload.getSender(), payload.getText()));
        }
    }

    void logUserSignOnOrOff(SystemPayload systemPayload) {
        if (isTriggeredByDifferentUser(systemPayload)) {
            if (systemPayload.getStatus().isSignIn()) {
                printStream.println(systemPayload.getUser() + " signed on (" + systemPayload.getTimestamp() + ")");
            } else {
                printStream.println(systemPayload.getUser() + " signed off (" + systemPayload.getTimestamp() + ")");
            }
        }
    }

    private boolean isTriggeredByDifferentUser(SystemPayload systemPayload) {
        return !systemPayload.getUser().equals(userContext.getUser());
    }
}
