package de.wschneider.chatclient;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class SystemPayload {

    private String user;
    private Status status;
    private String timestamp;

    public enum Status {
        SIGN_IN, SIGN_OFF;

        boolean isSignIn() {
            return this == SIGN_IN;
        }
    }
}
