package de.wschneider.chatclient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;

@SpringBootApplication(exclude = WebMvcAutoConfiguration.class)
public class ChatClientApplication implements CommandLineRunner {

	@Autowired
	private ChatClient chatClient;

	public static void main(String[] args) {
		SpringApplication springApplication = new SpringApplication(ChatClientApplication.class);
		springApplication.setWebApplicationType(WebApplicationType.NONE);
		springApplication.run(args);
	}

	@Override
	public void run(String... args) {
		chatClient.start();
	}
}
