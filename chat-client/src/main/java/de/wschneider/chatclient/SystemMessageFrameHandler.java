package de.wschneider.chatclient;

import java.lang.reflect.Type;

import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class SystemMessageFrameHandler implements StompFrameHandler {

    private final LinePrinter linePrinter;

    @Override
    public Type getPayloadType(StompHeaders headers) {
        return SystemPayload.class;
    }

    @Override
    public void handleFrame(StompHeaders headers, Object payload) {
        linePrinter.logUserSignOnOrOff((SystemPayload) payload);
    }
}
