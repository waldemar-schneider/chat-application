package de.wschneider.chatclient;

import java.lang.reflect.Type;

import org.springframework.messaging.simp.stomp.StompFrameHandler;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class ChatMessageFrameHandler implements StompFrameHandler {

    private final LinePrinter linePrinter;

    @Override
    public Type getPayloadType(StompHeaders headers) {
        return MessageResponsePayload.class;
    }

    @Override
    public void handleFrame(StompHeaders headers, Object payload) {
        linePrinter.logChat((MessageResponsePayload) payload);
    }
}
