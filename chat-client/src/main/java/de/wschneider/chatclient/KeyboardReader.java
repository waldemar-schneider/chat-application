package de.wschneider.chatclient;

import java.util.Scanner;

import org.springframework.stereotype.Component;

@Component
public class KeyboardReader {

    private Scanner keyboardInput = new Scanner(System.in);

    public String readInput() {
        return keyboardInput.nextLine();
    }
}
