package de.wschneider.chatclient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandlerAdapter;
import org.springframework.stereotype.Component;

@Component
public class ChatStompSessionHandler extends StompSessionHandlerAdapter {

    private static final Logger LOGGER = LoggerFactory.getLogger(ChatStompSessionHandler.class);

    private final ChatMessageFrameHandler chatMessageFrameHandler;
    private final SystemMessageFrameHandler systemMessageFrameHandler;

    @Value("${chat.application.topic}")
    private String topic;

    @Value("${chat.application.systemMessageTopic}")
    private String systemMessageTopic;

    public ChatStompSessionHandler(ChatMessageFrameHandler chatMessageFrameHandler, SystemMessageFrameHandler systemMessageFrameHandler) {
        this.chatMessageFrameHandler = chatMessageFrameHandler;
        this.systemMessageFrameHandler = systemMessageFrameHandler;
    }

    @Override
    public void afterConnected(StompSession session, StompHeaders connectedHeaders) {
        session.subscribe(topic, chatMessageFrameHandler);
        session.subscribe(systemMessageTopic, systemMessageFrameHandler);
    }

    @Override
    public void handleException(StompSession session, StompCommand command, StompHeaders headers, byte[] payload, Throwable exception) {
        LOGGER.error("Got an exception", exception);
    }

}