package de.wschneider.chatclient;

import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.messaging.WebSocketStompClient;

import lombok.AccessLevel;
import lombok.Setter;
import lombok.SneakyThrows;

@Component
public class ChatClient {

    private static final Logger LOG = LoggerFactory.getLogger(ChatClient.class);

    private final ChatStompSessionHandler chatStompSessionHandler;
    private final UserContext userContext;
    private final KeyboardReader keyboardReader;

    @Value("${chat.application.wsUrl}")
    private String wsUrl;

    @Value("${chat.application.port}")
    private int port;

    @Value("${chat.application.chatMapping}")
    @Setter(value = AccessLevel.PACKAGE)
    private String chatMapping;
    private WebSocketStompClient stompClient;

    @Autowired
    public ChatClient(ChatStompSessionHandler chatStompSessionHandler, UserContext userContext) {
        this.chatStompSessionHandler = chatStompSessionHandler;
        this.userContext = userContext;
        this.stompClient = new WebSocketStompClient(new StandardWebSocketClient());
        this.keyboardReader = new KeyboardReader();
    }

    ChatClient(ChatStompSessionHandler chatStompSessionHandler, UserContext userContext, KeyboardReader keyboardReader, WebSocketStompClient webSocketStompClient) {
        this.chatStompSessionHandler = chatStompSessionHandler;
        this.userContext = userContext;
        this.stompClient = webSocketStompClient;
        this.keyboardReader = keyboardReader;
    }

    @SneakyThrows
    public void start() {
        StompSession session = connectAndRetrieveSession();

        System.out.println("Please enter your name:");
        String name = keyboardReader.readInput();
        userContext.setUser(name);

        System.out.println("Hello " + name + "! Welcome to the chat app");
        System.out.println("What is on your mind?");
        session.send("/app/sign-in", name);

        handleInput(session, name);
    }

    void handleInput(StompSession session, String name) {
        boolean running = true;
        while (running) {
            String message = keyboardReader.readInput();

            if (message.trim().isEmpty()) {
                System.out.println("Please enter a valid message.");
            } else {
                if ("exit".equals(message)) {
                    running = shutdown(session, name);
                } else {
                    sendMessage(session, name, message);
                }
            }
        }
    }

    boolean shutdown(StompSession session, String name) {
        boolean running;
        userContext.setSignOffInitiated(true);
        session.send("/app/sign-off", name);
        System.out.println("Chat client will shutdown.");
        running = false;
        return running;
    }

    private void sendMessage(StompSession session, String name, String message) {
        session.send("/app" + chatMapping, new Message(name, message));
    }

    @SneakyThrows
    private StompSession connectAndRetrieveSession() {
        stompClient.setMessageConverter(new MappingJackson2MessageConverter());

        ListenableFuture<StompSession> futureStompSession = stompClient.connect(wsUrl + chatMapping, chatStompSessionHandler, port);
        futureStompSession
            .addCallback(session -> LOG.debug("Session created successfully."), ex -> LOG.error("Got an error:", ex));
        return futureStompSession.get(5, TimeUnit.SECONDS);
    }
}
