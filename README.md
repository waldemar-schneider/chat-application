# Coding challenge for Adobe

## notes about the challenge

### Notes about the code

- I needed to dive into WebSocket concepts as well as about the STOMP protocol.
- I never worked with WebSockets before. Therefor some of the solutions might look like a bit unusual as I am used to REST based services.
- I tried to reach a good coverage of LOCs as much as I could. 
- I kept the package structure simple as there was no need to seprate the classes due to the amount of classes.
- I used the given-when-then structure (comes via Domain Specific Language) for tests, to simplify reading of tests/code and to increase maintainability.
- I used a build script to stay flexible with docker commands without the maven xml hassle

#### Things I wanted to implement/improve (but struggled due to time and complexity)

- proper listing of existing users while signing on.
- proper formatting of outgoing/incoming chat messages (e.g. avoid duplicate outgoing messages).
- proper formatting of WebSockets tests as I was very new to this technology.
- I could not setup a test that the context is loading properly for the client.

## How to run build and run

In order to build and run the server you need to follow these steps:

1. Go into the `server` - directory
1. Execute the `build.sh` shell script (this triggers the maven build + creates a local docker image with the tag: `wschneider/chat-server`)
1. After the docker image is built, execute `docker run --rm -it -p 8000:8080 wschneider/chat-server`
1. Go to the client directory and trigger the maven build via `mvn package`
1. Start the command line client (or multiple ones) via `java -jar target/client.jar`
1. Start chatting (with multiple clients)

## Sources 

### input/help/inspiration:

- https://en.wikipedia.org/wiki/WebSocket
- https://stomp.github.io/
- https://docs.spring.io/spring-framework/docs/current/reference/html/web.html#websocket-stomp
- https://spring.io/guides/gs/messaging-stomp-websocket/
- https://www.baeldung.com/websockets-api-java-spring-client
- https://www.baeldung.com/websockets-spring
- https://www.baeldung.com/spring-boot-docker-images

### Inspiration/Help for testing:

- https://martinfowler.com/bliki/DomainSpecificLanguage.html
- https://martinfowler.com/bliki/GivenWhenThen.html
- https://github.com/spring-guides/gs-messaging-stomp-websocket/blob/main/complete/src/test/java/com/example/messagingstompwebsocket/GreetingIntegrationTests.java
- https://rieckpil.de/write-integration-tests-for-your-spring-websocket-endpoints/
- https://stackoverflow.com/questions/41767496/mock-same-method-with-different-parameters
- https://stackoverflow.com/questions/8504074/how-to-verify-multiple-method-calls-with-different-params